import numpy as np
import tensorflow.contrib.layers as layers
import tensorflow.contrib.metrics as metrics
import tensorflow as tf
import time
import threading
import ipdb

max_steps = 100
batch_size = 10
input_dim = 20
layer_dim = 128
queue_sleep_time = 1
number_gpus = 2

def load_enqueue(session, enqueue_op,  coord, input_control=1):
    step = 0

    while not coord.should_stop():
        print("{} creating data:{} ".format(threading.currentThread().getName(),step))
        indices = np.random.randint(2,size=batch_size)
        data = np.random.randn(batch_size, input_dim) * input_control + 20*indices[:,None]
        feed_dict = {feature_input:data, label_input:indices}
        session.run(enqueue_op, feed_dict=feed_dict)

        time.sleep(queue_sleep_time)
        step+=1
        print("{} data created:{}".format(threading.currentThread().getName(),step))
        print("should stop: {}".format(coord.should_stop()))
    coord.request_stop()
    print("thread completed work")

def inference(input_tensor, trainable=True):
    tensor_1 = layers.fully_connected(input_tensor,layer_dim, scope='fc1', trainable=trainable)
    return layers.fully_connected(tensor_1, 2, activation_fn=None, scope='fc2', trainable=trainable)

def loss_op(logits, labels):
    cross_entropy = tf.reduce_mean(
        tf.nn.softmax_cross_entropy_with_logits(logits,labels),name='cross_entropy')
    tf.add_to_collection('losses', cross_entropy)
    # The total loss is defined as the cross entropy loss plus all of the weight
    return tf.add_n(tf.get_collection('losses'), name='total_loss')

def tower_scope(scope):
    data,labels = queue.dequeue_many(batch_size)
    with tf.variable_scope('train'):
        logits = inference(data)
    loss_op(logits, labels)
    losses = tf.get_collection('losses', scope)
    total_loss = tf.add_n(losses, name='total_loss')
    loss_averages = tf.train.ExponentialMovingAverage(0.9, name='avg')
    loss_averages_op = loss_averages.apply(losses + [total_loss])
    with tf.control_dependencies([loss_averages_op]):
        total_loss = tf.identity(total_loss)
    return total_loss

def average_gradients(tower_grads):
    """Calculate the average gradient for each shared variable across all towers.
    Note that this function provides a synchronization point across all towers.
    Args:
      tower_grads: List of lists of (gradient, variable) tuples. The outer list
        is over individual gradients. The inner list is over the gradient
        calculation for each tower.
    Returns:
       List of pairs of (gradient, variable) where the gradient has been averaged
       across all towers.
    """
    average_grads = []
    for grad_and_vars in zip(*tower_grads):
        # Note that each grad_and_vars looks like the following:
        #   ((grad0_gpu0, var0_gpu0), ... , (grad0_gpuN, var0_gpuN))
        grads = []
        for g, _ in grad_and_vars:
            # Add 0 dimension to the gradients to represent the tower.
            expanded_g = tf.expand_dims(g, 0)

            # Append on a 'tower' dimension which we will average over below.
            grads.append(expanded_g)

        # Average over the 'tower' dimension.
        grad = tf.concat(0, grads)
        grad = tf.reduce_mean(grad, 0)

        # Keep in mind that the Variables are redundant because they are shared
        # across towers. So .. we will just return the first tower's pointer to
        # the Variable.
        v = grad_and_vars[0][1]
        grad_and_var = (grad, v)
        average_grads.append(grad_and_var)

    return average_grads


def assignment_op(source_variables, target_variables):
    assign_list = []
    for tr_var in source_variables:
        for inf_var in target_variables:
            tr_var_name = ''.join(tr_var.name.split('/')[1:])
            inf_var_name = ''.join(inf_var.name.split('/')[1:])
            if tr_var_name == inf_var_name:
                assign_list.append(inf_var.assign(tr_var))
    return tf.group(*assign_list, name='assignment_op')

def extract_values(session,variables):
    results = {}
    for var in variables:
       results[var.name] = var.eval(session=session)
    return results


with tf.Graph().as_default() as graph, tf.device('/cpu:0'):
    global_step = tf.get_variable('global_step', [], initializer=tf.constant_initializer(0), trainable=False)
    opt = tf.train.AdamOptimizer(0.9)
    feature_input = tf.placeholder(tf.float32, shape=[None, input_dim])
    label_input = tf.placeholder(tf.int32,shape=[None])
    queue = tf.RandomShuffleQueue(capacity=1000,min_after_dequeue=20,dtypes=[tf.float32, tf.float32],
                                  shapes=[(input_dim,), (2,)]
                                  )
    labels = tf.one_hot(label_input,depth=2, dtype=tf.float32)
    enqueue_op = queue.enqueue_many([feature_input, labels])
    close_op = queue.close()
    # tf.get_variable_scope().reuse_variables()
    with tf.variable_scope('inference'):
        logits = inference(feature_input, trainable=False)
    infer_op = tf.nn.softmax(logits,dim=-1)
    tower_grads = []

    for i in range(number_gpus):
        with tf.device("/gpu:{}".format(i)):
            with tf.name_scope('tower_{}'.format(i)) as scope:
                loss = tower_scope(scope)
                tf.get_variable_scope().reuse_variables()
                grads = opt.compute_gradients(loss)
                tower_grads.append(grads)

    grads = average_gradients(tower_grads)
    train_op = opt.apply_gradients(grads, global_step=global_step)

    train_variables = tf.trainable_variables()
    inference_variables = tf.get_collection(tf.GraphKeys.VARIABLES,scope='inference')

    assign_op = assignment_op(train_variables, inference_variables)

    init = tf.initialize_all_variables()

    sess = tf.Session(config=tf.ConfigProto(
        allow_soft_placement=True,
        operation_timeout_in_ms=20000,
        log_device_placement=False))

    sess.run(init)
    inital_training_var = extract_values(sess,train_variables)
    initial_infer_var = extract_values(sess, inference_variables)

    coord = tf.train.Coordinator()
    threads = [threading.Thread(target=load_enqueue, args=(sess,enqueue_op,coord,)) for i in range(5)]
    for t in threads: t.start()
    try:
        for step in range(max_steps):
            if coord.should_stop():
                break

            start_time = time.time()
            _, loss_value,q_size = sess.run([train_op, loss,queue.size()])
            duration = time.time() - start_time
            print("step: {} duration: {} loss_value:{} qsize:{}".format(step, duration, loss_value, q_size))
        coord.request_stop()
        # And wait for them to actually do it.
        coord.join(threads)
        # sess.run(close_op)
        sess.run(assign_op)

    except Exception as error:
        print("Done: {} ".format(error))
    input_control = 1
    final_train_variables = extract_values(sess, train_variables)
    final_infer_variables = extract_values(sess,inference_variables)
    indices = np.random.randint(2, size=batch_size*100)
    test_data = np.random.randn(batch_size*100, input_dim) * input_control + 20*indices[:,None]
    results = sess.run(infer_op,feed_dict={feature_input:test_data})
    mse = np.sqrt(np.mean((np.argmax(results,1) - indices)**2))
    sess.close()
    print("rmse: {}".format(mse))
